
const express = require('express');


module.exports = (app) => {
  const router = express.Router();
  // TODO create the swagger documentation

  router.post('/calcular-preco-prazo', app.preventBreak(app.controllers.correios.calcularPrecoPrazo));


  return router;
};
