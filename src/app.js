const app = require('express')();
const consign = require('consign');

app.preventBreak = require('@igorpdasilvaa/controller-handler');



consign({ cwd: 'src' })
  .include('./config/dotenv.js')
  .then('./config/passport.js')
  .then('./config/swagger.js')
  .then('./config/middlewares.js')
  .then('./errors')
  .then('./services')
  .then('./controllers')
  .then('./routes')
  .then('./config/router.js')
  .into(app);


app.get('/', (_, res) => res.status(200).send('Api running normally'));


module.exports = app;
