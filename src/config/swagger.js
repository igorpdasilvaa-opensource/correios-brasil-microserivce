const port = process.env.PORT;
const swaggerJSDoc = require('swagger-jsdoc');

// TODO pick the swagger definition from package.json and from the .env

const swaggerDefinition = {
  info: {
    title: 'Nodejs skeleton',
    version: '0.0.1',
    description: 'This is the REST API',
  },
  host: `localhost:${port}`,
  basePath: '/',
};

const options = {
  swaggerDefinition,
  explorer: true,
  apis: ['src/config/router.js', 'src/routes/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);

module.exports = swaggerSpec;
