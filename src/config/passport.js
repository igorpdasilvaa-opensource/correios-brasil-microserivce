const passport = require('passport');
const passportJwt = require('passport-jwt');

const secret = process.env.jwtsecret;

const { Strategy, ExtractJwt } = passportJwt;

module.exports = () => {
  const params = {
    secretOrKey: secret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  };

  const strategy = new Strategy(params, async (payload, done) => {
    try {
      if (payload.token === process.env.appToken) { done(null, { ...payload }); } else done(null, false);
    } catch (error) {
      done(error, false);
    }
  });

  passport.use(strategy);

  return {
    authenticate: () => passport.authenticate('jwt', { session: false }),
  };
};
