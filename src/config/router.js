const express = require('express');
const swaggerUi = require('swagger-ui-express');

module.exports = (app) => {
  const unProtectedRouter = express.Router();

  const protectedRouter = express.Router();
  unProtectedRouter.use('/correios', app.routes.correios);

  app.use('/v1', unProtectedRouter);
  app.use('/v1', app.config.passport.authenticate(), protectedRouter);
  app.use('/docs', swaggerUi.serve, swaggerUi.setup(app.config.swagger));
};
