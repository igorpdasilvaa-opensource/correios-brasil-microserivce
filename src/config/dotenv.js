const dotenv = require('dotenv');
const path = require('path');

const envFileContent = dotenv.config().parsed;
console.log('----------------------DEBUG----------------')
console.log(`${process.cwd()}`)
console.log(`${dotenv.config()}`)
const envFilePathToRewrite = path.resolve(process.cwd(), `.env.${envFileContent.ENVIRONMENT}`);
dotenv.config({ path: envFilePathToRewrite });
