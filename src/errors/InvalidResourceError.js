module.exports = () => function InvalidResourceError(message = 'This resource does not belong to the user') {
  this.name = 'InvalidResourceError';
  this.message = message;
  this.httpStatus = 403;
};
