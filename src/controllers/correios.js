
module.exports = (app) => {
  const calcularPrecoPrazo = async (req, res) => {
    const precoPrazo = await app.services.correios.calcularPrecoPrazo(req.body);
    return res.status(200).json(precoPrazo)
  };

  return { calcularPrecoPrazo };
};
