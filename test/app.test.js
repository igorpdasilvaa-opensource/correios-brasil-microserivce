const request = require('supertest');
const app = require('../src/app');

test('healthcheck must suceed returning 200 and the message: Api running normally', async () => {
  const res = await request(app).get('/');
  expect(res.status).toBe(200);
  expect(res.text).toBe('Api running normally');
});