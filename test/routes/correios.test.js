const request = require('supertest');
const jwt = require('jwt-simple');
const app = require('../../src/app');

const dadosCalculo = {
  // Não se preocupe com a formatação dos valores de entrada do cep, qualquer uma será válida (ex: 21770-200, 21770 200, 21asa!770@###200 e etc),
  sCepOrigem: "81200100",
  sCepDestino: "21770200",
  nVlPeso: 1,
  nCdFormato: 1,
  nVlComprimento: 20,
  nVlAltura: 20,
  nVlLargura: 20,
  nCdServico: ["04014", '04510'], //Array com os códigos de serviço
  nVlDiametro: 0,
};

test('Deve calcular o preco e prazo com sucesso', () => request(app).post('/v1/correios/calcular-preco-prazo')
  .send(dadosCalculo).then((res) => {
    expect(res.status).toBe(200);
    expect(res.body[0]).toHaveProperty('Valor');
    expect(res.body[1]).toHaveProperty('Valor');
  }));

test('Deve Falhar na validação do formulário de consulta de preco e prazo', () => request(app).post('/v1/correios/calcular-preco-prazo')
  .send({ ...dadosCalculo, nVlPeso: "Pesado" }).then((res) => {
    expect(res.status).toBe(400);
    expect(res.body.error).toBe('should be number');
  }));
